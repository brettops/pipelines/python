import argparse

from ._version import __version__

parser = argparse.ArgumentParser()
parser.add_argument("name", help="Name")
parser.add_argument(
    "-V", "--version", action="version", version="%(prog)s " + __version__
)


def main():
    args = parser.parse_args()
    print("Hello", args.name)


if __name__ == "__main__":
    main()
