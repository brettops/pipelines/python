# Python Pipeline

<!-- BADGIE TIME -->

[![brettops pipeline](https://img.shields.io/badge/brettops-pipeline-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/brettops/pipelines/python?branch=main)](https://gitlab.com/brettops/pipelines/python/-/commits/main)
[![coverage report](https://img.shields.io/gitlab/pipeline-coverage/brettops/pipelines/python?branch=main)](https://gitlab.com/brettops/pipelines/python/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/brettops/pipelines/python)](https://gitlab.com/brettops/pipelines/python/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: black](https://img.shields.io/badge/code_style-black-000000.svg)](https://github.com/psf/black)
[![imports: isort](https://img.shields.io/badge/imports-isort-1674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Build, test, and release Python packages.

- Enforces code style with [black](https://black.readthedocs.io/en/stable/)

- Sorts module imports with [isort](https://pycqa.github.io/isort/)

- Validates package readiness with [pyroma](https://github.com/regebro/pyroma)

- Unit tests with [pytest](https://pytest.org)

- Builds wheels

- Releases to a private Python package index

## Usage

### Project Setup

At a minimum, this pipeline requires that:

- A `setup.py` is created

- A pytest unit test is defined somewhere

- A package directory is created; `PYTHON_PACKAGE_DIR` is defined

- Tags are protected

## Includes

A pipeline with everything enabled:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - black.yml
      - isort.yml
      - mypy.yml
      - pyroma.yml
      - pytest.yml
      - setuptools.yml
      - twine.yml
      - vulture.yml
```

### `black.yml`

Enable the Black code formatter:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - black.yml
```

### `isort.yml`

Enable the isort import sorter:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - isort.yml
```

### `mypy.yml`

Run mypy static type checker:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - mypy.yml
```

### `pyroma.yml`

Run pyroma package linter:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - pyroma.yml
```

### `pytest.yml`

Run pytest unit tests:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - pytest.yml
```

### `setuptools.yml`

Build a package with setuptools:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - setuptools.yml
```

### `twine.yml`

Publish the package to a PyPI repository:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - twine.yml
```

### `vulture.yml`

Find dead code with vulture:

```yaml
include:
  - project: brettops/pipelines/python
    file:
      - vulture.yml
```

### Deprecated includes

#### `lint.yml`

Lint Python code:

```yaml
include:
  - project: brettops/pipelines/python
    file: lint.yml
```

#### `package.yml`

Build and publish a Python package:

```yaml
include:
  - project: brettops/pipelines/python
    file: package.yml
```

#### `test.yml`

Test and lint Python code:

```yaml
include:
  - project: brettops/pipelines/python
    file: test.yml
```

## Publish the package

By default, this pipeline publishes to GitLab's PyPI Package Registry.

The pipeline can be used to publish to PyPI by setting the following variables:

- `PYTHON_PYPI_UPLOAD_URL` = `https://upload.pypi.org/legacy/`
- `PYTHON_PYPI_USERNAME` = `__token__`
- `PYTHON_PYPI_PASSWORD` - Your API token

## Using Packages

### Locally

Create the config directory:

```
mkdir -p ~/.config/pip
```

Create a `pip.conf` file:

```
$ cat ~/.config/pip/pip.conf
[global]
index-url = https://__token__:<read_api-token>gitlab.com/api/v4/groups/<group-id>/-/packages/pypi/simple
```

Install a package. GitLab will pull from [pypi.org](https://pypi.org) if the
package is not found.

```
$ pip install python-decouple
Looking in indexes: https://__token__:****@gitlab.com/api/v4/projects/32517826/packages/pypi/simple
Collecting python-decouple
  Downloading python_decouple-3.5-py3-none-any.whl (9.6 kB)
Installing collected packages: python-decouple
Successfully installed python-decouple-3.5
```

### In CI

The most sensible way to use packages from the GitLab Package Registry is to
pull them from the root namespace registry. This ensures that any packages your
user may have access to are available for use.

The `CI_JOB_TOKEN` cannot be used to determine the group ID by name, so it must
be provided upfront. This is preferred to using a deploy token, so that the
permissions are specific to your user and not blanket.

Define the `PYTHON_PYPI_GITLAB_GROUP_ID` variable in your CI file, or ask a
group owner to add it to CI variables.

## Variables

The following variables are required:

### `PYTHON_PACKAGE`

Python package directory. Defaults to `$CI_PROJECT_NAME`. This pipeline assumes
a single project contains a single package.

Example:

```yaml
PYTHON_PACKAGE: python_pipeline
```

Must be specified in any of the following situations:

- The package directory name and project name do not match.

- The project name contains characters other than `[a-zA-Z0-9_]`.

### `PYTHON_PYPI_GITLAB_GROUP_ID` / `PYTHON_PYPI_GITLAB_PROJECT_ID`

Pull packages from the GitLab Package Registry for this group / project ID. When
provided, this pipeline will be automatically configured to proxy PyPI packages
through GitLab.

This information must be provided because it cannot be obtained by
`CI_JOB_TOKEN` permissions.

### `PYTHON_PYPI_UPLOAD_URL` / `PYTHON_PYPI_DOWNLOAD_URL`

The URLs for private PyPI repositories to use. The upload URL defaults to the
GitLab PyPI Package Registry for the current project.

Supplying the download URL will override any other download configuration.

This pipeline expects the upstream PyPI repository to supporting dependency
proxying.

### `PYTHON_PYPI_USERNAME`

The username to access the private PyPI repository URL. Defaults to
`gitlab-ci-token`.

### `PYTHON_PYPI_PASSWORD`

The password to access the private PyPI repository URL. Defaults to
`$CI_JOB_TOKEN`.

## Further Reading

- <https://pypi.org/classifiers/>

- <https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/>

- <https://twine.readthedocs.io/en/latest/>

- <https://pip.pypa.io/en/stable/topics/configuration/>

- <https://docs.gitlab.com/ee/user/packages/pypi_repository/>

- <https://docs.gitlab.com/ee/user/permissions.html>
